---
title: "Task #5"
---

# [Task #5. Виртуальная файловая система. Програмный интерфейс. Ресурсы](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

Currency converter.

---

## References

- [procfs demo](https://gitlab.com/gl-khpi/examples/tree/master/procfs_rw).
- [sysfs demo](https://gitlab.com/gl-khpi/examples/tree/master/sysfs).

---

## Guidance

> [Использовать](https://gl-khpi.gitlab.io/#commit-requirements) Git.
>
> Использовать следующие названия директорий: `task05` - для проектов решений подзадач; `partXX` - для проекта решения соответствующей подзадачи (`XX` - номер подзадачи); `src` - для исходников.
>
> Периодически отправлять изменения в удаленный репозиторий, соответствующий Вашей фамилии.

:one: Implement the currency converter as a kernel module to...

- convert currency from/to Ukrainian hryvnia to/from euro.
- set the conversion factor based on the hryvnia.

:two: Add the interfaces into the module:

- *procfs* for currency conversion.
- *sysfs* to set conversion factors corresponding to the currencies used.

:heart: Implement bash script for the module testing.

:yellow_heart: Implement a C application to test module functionality.

:green_heart: Provide a test scenario.

---

## Extra

Add the following functionality to the kernel module:

- conversion logging;
- dynamically add a new type of currency for conversion.

<br>

