---
title: "Task #3"
---

# [Task #3. C Programming in Linux. GCC, CLANG. Linux kernel coding style](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

На языке Си написать программу-упаковщик и программу-распаковщик.

---

## References

- [Run-length encoding (RLE)](https://en.wikipedia.org/wiki/Run-length_encoding).
- [Easy way to create a Debian package](https://linuxconfig.org/easy-way-to-create-a-debian-package-and-local-package-repository).

---

## Guidance

> Использовать Git в соответствие с [требованиями](https://gl-khpi.gitlab.io/#commit-requirements).
>
> Для реализации новой функциональности использовать **отдельные** ветки.
>
> Соблюдать [Linux kernel coding style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).

1. В директории клонированного ранее репозитория, соответствующего Вашей фамилии, создать директорию проекта: `task03`.

2. В директории проекта расположить `Makefile`.

3. Использовать следующие названия файлов и директорий:
	- `src` и `bin` - директории для расположения исходников и результатов компиляции соответственно.
	- `pak` и `upak` - исполняемые файлы упаковщика и распаковщика соответственно.
	- `/opt/your_app_name` - директория для установки.

4. С помощью простого алгоритма сжатия (например, RLE) обработать данные стандартного входного потока, результат вывести в стандартный выходной поток.

5. Продемонстрировать обработку файлов, используя перенаправление [ввода-вывода](https://ru.wikipedia.org/wiki/%D0%9F%D0%B5%D1%80%D0%B5%D0%BD%D0%B0%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2%D0%B2%D0%BE%D0%B4%D0%B0-%D0%B2%D1%8B%D0%B2%D0%BE%D0%B4%D0%B0).

---

## Extra

- Создать `Makefile` для сборки (all), установки (install), удаления (uninstall), очистки рабочего каталога (clean), проверки функциональности (test).

- По команде `make install` реализовать сборку простейшего [deb-пакета](https://en.wikipedia.org/wiki/Deb_(file_format)) для его последующей установки командй `sudo dpkg -i file_name.deb`.

<br>

