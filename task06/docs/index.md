---
title: "Task #6"
---

# [Task #6. Драйвер символьного устройства](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

Implement a character device driver for text messaging between users.

---

## References

- [procfs demo](https://gitlab.com/gl-khpi/examples/tree/master/procfs_rw).
- [sysfs demo](https://gitlab.com/gl-khpi/examples/tree/master/sysfs).
- [Использование параметров при загрузке модуля](https://www.ibm.com/developerworks/ru/library/l-linux_kernel_11/index.html).
- [Модуль как драйвер. Пример реализации](https://www.ibm.com/developerworks/ru/library/l-linux_kernel_19/index.html?ca=drs-).

---

## Guidance

> Use the following names: `Your.Name` - as a home directory of your projects; `task06` - as a directory of the current project; `partXX` - as a directory of the corresponding subtask (`XX` - subtask number); `src` - as a sources directory.

:one: Implement a character device driver with the following requirements:

- 1kB buffer size;
- buffer increase as a module parameter;
- should be available for all users;
- works only ASCII strings.

:two:  Add the interfaces into the driver:

- `sysfs` for buffer clean up;
- `procfs` for displaying used buffer volume and buffer size.

:heart: Implement bash script for the driver testing. 

:yellow_heart: Provide a test scenario.

---

## Extra

Implemented character device should be committed to [GitLab repository](https://gitlab.com/gl-khpi/practice). During implementation, please follow to [code](https://www.kernel.org/doc/html/latest/process/coding-style.html) and [git](https://gl-khpi.gitlab.io/#commit-requirements) style.

!!! hint "Project Workflow"

	- [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [repo](https://gitlab.com/gl-khpi/practice).
	- Clone that fork to a local repository.
	- Add the [repo](https://gitlab.com/gl-khpi/practice) as remote to your local clone (e.g. as `main`).
	- Your main branch is `main/your.name`, you’ll share your solutions as MRs to this branch:

		- Create a branch and add commits there. Push this branch to remote:
			```bash
			git checkout -b branch_name
			# add commits here
			git push -u origin HEAD:branch_name
			```
		- Create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) in *GitLab* web interface from `branch_name` in your fork to `your.name` in the [repo](https://gitlab.com/gl-khpi/practice).

??? hint "Merge the latest changes from `main/master` branch"

	```bash
	git checkout your.name
	git fetch main
	git merge main/master
	# Resolve conflicts
	git push
	```

<br>

