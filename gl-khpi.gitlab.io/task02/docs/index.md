---
title: "Task #2"
---

# [Task #2. Git: main rules, commands, branching, merging](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

Написать bash-скрипт, который реализует игру **Угадай число**.

---

## References

- [Shell Functions](https://www.gnu.org/software/bash/manual/bash.html#Shell-Functions).
- [Bash Variables - RANDOM](https://www.gnu.org/software/bash/manual/bash.html#Bash-Variables).
- [Git Merge: Resolving Conflicts](https://githowto.com/resolving_conflicts).
- [Using Meld as your git difftool and mergetool](https://stackoverflow.com/questions/34119866/setting-up-and-using-meld-as-your-git-difftool-and-mergetool).

---

## Guidance

> В директории клонированного ранее репозитория, соответствующего Вашей фамилии, создать директорию проекта: `task02`.
>
> Для каждого последующего шага выполнять **не менее одного** коммита в соответствии с [требованиями](https://gl-khpi.gitlab.io/#commit-requirements); для реализации новой функциональности обязательно использовать **отдельные** ветки.
>
> Периодически отправлять изменения в удаленный репозиторий.

1. Создать пустой файл `readme.md`.

2. Добавить в `readme.md` одну строку с общей формулировкой задачи, например:
    ```
    Task: write a Guess the Number game using the Bash script.
    ```

3. Добавить в `readme.md` одну строку с формулировкой подзадачи, например:
    ```
    Subtask: create a random number generator script.
    ```

4. В **новой** ветке написать скрипт, который генерирует и выводит случайное число в диапазоне от **0** до **5**.
	- В файл `readme.md` добавить заголовки всех выполненных в данной ветви коммитов.
	- В ветке **master**, в файл `readme.md` добавить строку:
	    ```
        Subtask completed.
        ```
	- В ранее созданной ветке выполнить **merge** с веткой **master**, разрешить конфликты с учётом хронологии выполненных действий, а затем выполнить **merge** в **master**.

5. В ветке **master**, в файл `readme.md` добавить строку с формулировкой следующей подзадачи, например:
    ```
    Subtask: Extract Function refactoring.
    ```

6. В **новой** ветке выполнить [рефакторинг](https://refactoring.com/catalog/extractFunction.html): реализовать генерацию случайного числа в указанном диапазоне в виде отдельной [функции](https://www.gnu.org/software/bash/manual/bash.html#Shell-Functions).
	- В файл `readme.md` добавить заголовки всех выполненных в данной ветви коммитов.
	- В ветке **master**, в файл `readme.md` добавить строку `Subtask completed.`.
	- В ранее созданной ветке выполнить **merge** с веткой **master**, разрешить конфликты с учётом хронологии выполненных действий, а затем выполнить **merge** в **master**.

7. В ветке **master**, в файл `readme.md` добавить строку с формулировкой следующей подзадачи.

8. В **новой** ветке реализовать сравнение сгенерированного случайно числа **X** с числом **Y**, которое задано первым параметром командной строки и вывести соответствующее сообщение: *Y is less than X*, *Y is greater than X* или *Y is equal to X*.
	- В файл `readme.md` добавить заголовки всех выполненных в данной ветви коммитов.
	- В ветке **master**, в файл `readme.md` добавить строку `Subtask completed.`.
	- В ранее созданной ветке выполнить **merge** с веткой **master**, разрешить конфликты с учётом хронологии выполненных действий, а затем выполнить **merge** в **master**.

9. В ветке **master**, в файл `readme.md` добавить строку с формулировкой следующей подзадачи. Далее файл `readme.md` изменять по наработанному сценарию.

10. Реализовать ввод с подсказкой числа, если оно не задано параметром командной строки.

11. Реализовать возможность задания значения верхней границы диапазона случайных чисел вторым параметром командной строки в виде целого числа **от 1 до 100**.

12. Реализовать ввод с подсказкой значения верхней границы, если не задан второй параметр командной строки.

13. Реализовать возможность указания числа попыток угадывания третьим параметра командной строки.

14. Реализовать ввод с подсказкой числа попыток угадывания, если не задан третий параметр командной строки.

15. Выполнить рефакторинг, добавить комментарии для основных действий скрипта.

16. Реализовать возможность повторной игры с теми же параметрами при успешном отгадывания числа.

---

## Extra

- Реализовать задание значения верхней границы и числа попыток в виде соответствующих ключей. Например: `--upper 9 --retries 3` или `-r 2 -u 5`.
- Реализовать меню:
	- играть;
	- параметры;
		- количество попыток;
		- наибольшее число.
	- выход.

---

## Notes

!!! quote "Resolving merge conflicts"

	- Merge the master branch with another branch:
		```bash
		git checkout <another_branch>
		git merge master
		```
	- Edit conflicting files to resolve the conflict manually.
	- Make a commit of conflict resolution:
		```bash
		git add <edited_files>
		git commit -m "Merged master: Fix conflict"
		```

!!! quote "Removing local untracked files"

	- Show which files will be deleted: `git clean -n`
	- Remove untracked files from the working tree: `git clean -f`
	- Remove files and directories: `git clean -fd`

<br>

