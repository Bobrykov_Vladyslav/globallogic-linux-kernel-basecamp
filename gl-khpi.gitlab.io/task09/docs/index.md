---
title: "Task #9"
---

# [Task #9. Concurrency and Synchronization](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

Just spam it, baby!

---

## Guidance

> Use the following names: `Your.Name` - as a home directory of your projects; `task09` - as a directory of the current project; `src` - as a sources directory.

Написать модуль ядра, обеспечивающий одновременный вывод заданных сообщений через заданные интервалы времени.

1) Каждое сообщение выводить в отдельном потоке.

2) Стартовать вывод - по нажатию кнопки, подключённой к выводу GPIO (или любая клавиша стандартной клавиатуры).

3) Параметры вывода каждого сообщения задавать через `sysfs/procfs`:

- сообщение,
- задержка между каждым выводом сообщения (в ms),
- общее время вывода повторяющегося сообщения (в ms).

---

## Extra

:one: Использовать фиксированный пул тредов-исполнителей и очередь заданий (списки).

:two: Сигнализировать начало вывода - зажечь на 500 ms светодиод, подключённый к выводу GPIO.

??? hint "Project Workflow"

	- [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [repo](https://gitlab.com/gl-khpi/practice).
	- Clone that fork to a local repository.
	- Add the [repo](https://gitlab.com/gl-khpi/practice) as remote to your local clone (e.g. as `main`).
	- Your main branch is `main/your.name`, you’ll share your solutions as MRs to this branch:

		- Create a branch and add commits there. Push this branch to remote:
			```bash
			git checkout -b branch_name
			# add commits here
			git push -u origin HEAD:branch_name
			```
		- Create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) in *GitLab* web interface from `branch_name` in your fork to `your.name` in the [repo](https://gitlab.com/gl-khpi/practice).

<br>

