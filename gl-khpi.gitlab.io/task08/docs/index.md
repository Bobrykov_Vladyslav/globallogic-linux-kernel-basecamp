---
title: "Task #8"
---

# [Task #8. Memory Management](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

Multi-user reminders.

---

## Guidance

> Use the following names: `Your.Name` - as a home directory of your projects; `task08` - as a directory of the current project; `src` - as a sources directory.

Написать модуль ядра, обеспечивающий:

- установку напоминания: указать имя пользователя, текст сообщения, задержку в секундах относительно текущего момента;
- отмену напоминания: указать имя пользователя, номер напоминания;
- просмотр списка оставшихся напоминаний для всех пользователей;
- просмотр списка оставшихся напоминаний для конкретного пользователя: указать имя пользователя. 

> **Требования**
> 
> - Для реализации использовать [списки](https://gitlab.com/gl-khpi/examples/-/tree/master/memory/list).
> - Использовать не более одного таймера.

??? hint "Project Workflow"

	- [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [repo](https://gitlab.com/gl-khpi/practice).
	- Clone that fork to a local repository.
	- Add the [repo](https://gitlab.com/gl-khpi/practice) as remote to your local clone (e.g. as `main`).
	- Your main branch is `main/your.name`, you’ll share your solutions as MRs to this branch:

		- Create a branch and add commits there. Push this branch to remote:
			```bash
			git checkout -b branch_name
			# add commits here
			git push -u origin HEAD:branch_name
			```
		- Create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) in *GitLab* web interface from `branch_name` in your fork to `your.name` in the [repo](https://gitlab.com/gl-khpi/practice).

<br>

